# easymqtt - A ridiculously simple MQTT wrapper library

I wanted to make creating small MQTT based services (microservices) easier,
specifically the handing of subscriptions and message callbacks. easymqtt is a 
library to do that. It subclasses the paho.mqtt.client.Client
class and provides decorators for subscription, connection and disconnection.
It's less than 20 lines of code.

## Example:

```python
from easymqtt import EasyMQTT

mqtt = EasyMQTT()
mqtt.connect("localhost")

@mqtt.subscription("test")
def test_handler(client, userdata, msg):
    print(msg.topic, msg.payload)
    
mqtt.loop_forever()
```

There is another example available in [example.py](example.py).

## Installation

It's on PyPI so just run:

    pip install easymqtt

## The Future

I've not ruled out adding more features to streamline MQTT usage, so feature
requests are welcome.
