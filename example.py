from easymqtt import EasyMQTT
import time

mqtt = EasyMQTT()

@mqtt.connection
def on_connect(client, userdata, flags, rc):
    print("connected")

@mqtt.disconnection
def on_disconnect(client, userdata, rc):
    print("disconnected")

mqtt.connect("localhost", 1883)

@mqtt.subscription("test/test")
def my_callback(client, userdata, msg):
    print(msg.topic, msg.payload)

mqtt.loop_start()

time.sleep(10)
mqtt.disconnect()
mqtt.loop_stop()

